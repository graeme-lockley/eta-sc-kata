module Main where

import Kata.SC

import Data.Char
import Data.List

import Test.Hspec
import Test.Hspec.QuickCheck
import Test.QuickCheck


customCharacterSeparatorGenerator :: Gen Char
customCharacterSeparatorGenerator =
  elements $ filter (\c -> c /= '-' && c /= '[' && (not . isDigit) c) [chr 32 .. chr 127]


naturalNumberGenerator :: Gen Int
naturalNumberGenerator =
  choose (0, 2000)


main =
  hspec $ do
    describe "add" $ do
      prop "natural numbers separated with a comma or newline returns the sum of all numbers less than 1001" $
        forAll (listOf naturalNumberGenerator) $ \ns -> do
          separators <- infiniteListOf $ elements [",", "\n"]
          return $ add (mkInput separators ns) `shouldBe` sumList ns

      prop "natural numbers separated with a custom character returns the sum of all numbers less than 1001" $
        forAll (listOf naturalNumberGenerator) $ \ns ->
          forAll customCharacterSeparatorGenerator $ \sep ->
            add ("//" ++ [sep] ++ "\n" ++ (intercalate [sep] $ map show ns)) `shouldBe` sumList ns

      prop "natural numbers separated with multiple custom multi-character separators returns sum of all numbers less than 1001" $
        forAll (listOf naturalNumberGenerator) $ \ns ->
          forAll (listOf1 $ listOf1 $ customCharacterSeparatorGenerator) $ \seps -> do
            separators <- infiniteListOf $ elements seps
            return $ add ("//[" ++ (intercalate "][" seps) ++ "]\n" ++ (mkInput separators ns)) `shouldBe` sumList ns

      prop "integers with at least one negative returns an error containing all of the negative values" $
        forAll (suchThat (listOf arbitrary) (any (< 0))) $ \ns ->
          add (intercalate "," $ map show ns) `shouldBe` (Left $ filter (< 0) ns)


sumList :: [Int] -> Either whatever Int
sumList =
  Right . sum . filter (< 1001)


mkInput :: [String] -> [Int] -> String
mkInput _ [] = ""
mkInput separators (x:xs) = mkInputHelper separators xs (show x)
  where mkInputHelper _ [] result =
          result
        mkInputHelper (s:ss) (n:ns) result =
          mkInputHelper ss ns (result ++ s ++ (show n))
